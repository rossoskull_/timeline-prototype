import React from 'react'
import Draggable from 'react-draggable'

const GridItem = ({id, start, duration, text, xstart, width, dragEvent, dragStartEvent, snap}) => {

	let styles = {}

	if (start && duration && width) {
		styles = {
			gridRowStart: start,
			gridRow: `${start} / span ${duration}`,
			gridColumn: `${xstart} / span ${width}`
		}
	}

	return (
		<Draggable
			axis="y"
			onStart={dragStartEvent}
			onDrag={dragEvent}
			scale={1} >
			<div draggable id={`grid-item-${id}`} style={styles} data-block-start={start} data-block-id={id} >
				{text}
			</div>
		</Draggable>
	)
}

export default GridItem