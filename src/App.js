import React from 'react';
import './App.css';
import GridItem from './GridItem'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      zoom: 2,
      list: [
        {id: 1, text: 'Hello', start: 1, duration: 6, xstart: 1, width: 1},
        {id: 2, text: 'Hello', start: 1, duration: 6, xstart: 2, width: 1},
        {id: 3, text: 'Hello', start: 1, duration: 6, xstart: 3, width: 1},
        {id: 4, text: 'Hello', start: 1, duration: 6, xstart: 4, width: 1},
        {id: 5, text: 'Hello', start: 1, duration: 6, xstart: 5, width: 1},
        {id: 6, text: 'Hello', start: 1, duration: 6, xstart: 6, width: 1},
        {id: 7, text: 'Hello', start: 1, duration: 6, xstart: 7, width: 1},
        {id: 8, text: 'Hello', start: 1, duration: 6, xstart: 8, width: 1},
        {id: 9, text: 'Hello', start: 1, duration: 6, xstart: 9, width: 1},
        {id: 10, text: 'Hello', start: 1, duration: 6, xstart: 10, width: 1},
        {id: 11, text: 'Hello', start: 1, duration: 8, xstart: 11, width: 1},
        {id: 12, text: 'Hello', start: 1, duration: 10, xstart: 12, width: 1},
        {id: 13, text: 'Hello', start: 7, duration: 6, xstart: 1, width: 10},
        {id: 14, text: 'Far', start: 20, duration: 16, xstart: 1, width: 12}
      ]
    }
  }

  zoomIn = () => {
    this.setState({
      zoom: this.state.zoom + 1
    })
  }

  zoomOut = () => {
    if (this.state.zoom > 1) {
      this.setState({
        zoom: this.state.zoom - 1
      })
    }    
  }

  EnableZoom = e => {
    window.addEventListener('wheel', e => {
      if (e.shiftKey) {
        if (e.deltaY > 0) {
          this.zoomOut()
        } else {
          this.zoomIn()
        }
      }
    })
  }

  componentDidMount() {

    let wrapper = document.querySelector('.wrapper')

    wrapper.addEventListener('mouseover', () => {
      wrapper.addEventListener('wheel', this.EnableZoom)
    })

    wrapper.addEventListener('mouseout', () => {
      wrapper.removeEventListener('wheel', this.EnableZoom);
    })
      
  }

  dragStartEvent = e => {
    // e.nativeEvent.dataTransfer.setData('text', 'anything')
    let start = e.target.getAttribute('data-block-start')
    let id = parseInt(e.target.getAttribute('data-block-id'))
    let element = this.state.list.find(element => element.id === id)
    console.log(start)
    this.setState({
      currentDraggedItem: id,
      currentStart: start,
      currentPageY: e.pageY,
      currentDraggedItemIndex: this.state.list.indexOf(element)
    })
  }

  dragEvent = e => {
    let newY = e.pageY
    let oldY = this.state.currentPageY
    let delta = newY - oldY

    let step = this.state.zoom

    console.log(delta, oldY, newY, step,  delta % (step) === 0)

    if (delta > 0 && delta % step === 0) {
      let {list} = this.state
      console.log("Old start", list[this.state.currentDraggedItemIndex].start)
      list[this.state.currentDraggedItemIndex].start += 1

      this.setState({
        list: list,
        currentPageY: newY
      }, () => {
        console.log(this.state.list[this.state.currentDraggedItemIndex].start)
      })
    } else if (delta > 0 && delta % step === 0) {
      let {list} = this.state
      console.log("Old start", list[this.state.currentDraggedItemIndex].start)
      list[this.state.currentDraggedItemIndex].start -= 1

      this.setState({
        list: list,
        currentPageY: newY
      }, () => {
        console.log(this.state.list[this.state.currentDraggedItemIndex].start)
      })
    }
     
    // if (delta > 0 && delta / step > 0) {
    //   let {list} = this.state
    //   list[this.state.currentDraggedItemIndex].start = this.state.currentStart + delta % (step)
    //   this.setState({
    //     list: list,
    //     currentPageY: newY
    //   }, () => {
    //     console.log(this.state.list[this.state.currentDraggedItemIndex].start)
    //   })
    // } else if (delta < 0 && delta / step < 0) {
    //   let {list} = this.state
    //   list[this.state.currentDraggedItemIndex].start = this.state.currentStart - delta % step
    //   this.setState({
    //     list: list,
    //     currentPageY: newY
    //   }, () => {
    //     console.log(this.state.list[this.state.currentDraggedItemIndex].start)
    //   })
    // }
  }

  render() {

    let {list} = this.state

    // console.log(list)

    let gridList = list.map(l => {
      return (<GridItem id={l.id} key={l.id} text={l.text + l.id} start={l.start} duration={l.duration} width={l.width} xstart={l.xstart} dragEvent={this.dragEvent} dragStartEvent={this.dragStartEvent} snap={this.state.zoom * 10 + 3} />)
    })

    let sideList = [<div key={0} className="timestamp"></div>]

    for (let i = 1; i < 51; i++) {
      sideList.push(<div key={i} className="timestamp"></div>)
    }

    return (
      <div className="App">
        <button onClick={this.zoomOut}>-</button><button onClick={this.zoomIn}>+</button>
        <div className="container">

          <div style={{gridAutoRows: `${this.state.zoom}px`}} className="sidebar">
            {sideList}
          </div>

          <div style={{gridAutoRows: `${this.state.zoom}px`}} className="wrapper">
            {gridList}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
